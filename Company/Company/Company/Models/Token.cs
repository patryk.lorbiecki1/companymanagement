﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Ekompania.Models
{
    public class Token
    {
        public string TokenId { get; protected set; }
        public long Expiry { get; protected set; }
    }
}