﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Ekompania.Models
{
    public class Kompania
    {
        public string Name { get; protected set; }
        public ISet<User> Uzytkownicy { get; protected set; }
 
        public Kompania(string name)
        {
            Name = name;
        }
    }
}