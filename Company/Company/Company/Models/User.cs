﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ekompania.Models
{
    public class User
    {
        public Guid Id { get; protected set; }
        public String Login { get; protected set; }
        public String Haslo { get; protected set; }
        public String Imie { get; protected set; }
        public String Nazwisko { get; protected set; }
        public String Stopien { get; protected set; }
        public String Pluton { get; protected set; }
        public Kompania Kompania { get; protected set; }
        public String QRCode { get; protected set; }

        public User(String login, String haslo, String imie, String nazwisko, String stopien)
        {
            Id = Guid.NewGuid();
            SetName(imie);
            SetSurname(nazwisko);
            SetLogin(login);
            SetHaslo(haslo);
         
        }
        private void SetName(string imie)
        {
            if (string.IsNullOrWhiteSpace(imie))
            {
                throw new ArgumentException("Pole nie może być puste oraz imię nie może zawierać spacji!");
            }
            Imie = imie;
        }

        private void SetSurname(string nazwisko)
        {
            if(string.IsNullOrWhiteSpace(nazwisko))
            {
                throw new ArgumentException("Pole nie może być puste oraz nazwisko nie może zawierać spacji!");
            }
            Nazwisko = nazwisko;
        }

        private void SetLogin(string login)
        {
            if(string.IsNullOrWhiteSpace(login))
            {
                throw new ArgumentException("Pole nie może być puste oraz login nie może zawierać spacji!");
            }
            Login = login;
        }
        private void SetHaslo(string haslo)
        {
            if(string.IsNullOrWhiteSpace(haslo))
            {
                throw new ArgumentException("Pole nie może być puste oraz hasło nie może zawierać spacji!");
            }
            Haslo = haslo;
        }

        private void SetStopien(string stopien)
        {
            if(string.IsNullOrWhiteSpace(stopien))
            {
                throw new ArgumentException("Pole nie może być puste oraz stopień nie może zawierać spacji!");
            }
            Stopien = stopien;
        }

    }

   
}