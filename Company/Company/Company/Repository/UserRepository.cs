﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ekompania.Models;

namespace Company.Repository
{
    public class UserRepository : IUserRepository
    {
        private static ISet<User> _users = new HashSet<User>
        {
            new User("user1", "tajne", "Kacper", "K", "st.kpr.pchor."),
            new User("user2", "tajne", "Mateusz", "K", "st.kpr.pchor."),
            new User("user3", "tajne", "Patryk", "L", "st.kpr.pchor."),
            new User("user4", "tajne", "Mateusz", "K", "szer.pchor."),
        };

        public async Task<User> GetUser(string login)
            => await Task.FromResult(_users.SingleOrDefault(x => x.Login == login));

        public async Task<User> GetUser(Guid id)
            => await Task.FromResult(_users.SingleOrDefault(x => x.Id == id));

        public async Task RemoveUser(string login)
        {
            var user = await GetUser(login);
            _users.Remove(user);
            await Task.CompletedTask;
        }

        public async Task AddUser(string login, string haslo, string imie, string nazwisko, string stopien)
            => await Task.FromResult(_users.Add(new User(login, haslo, imie, nazwisko, stopien)));

        public async Task<IEnumerable<User>> GetAllUsers()
            => await Task.FromResult(_users);
    }
}
