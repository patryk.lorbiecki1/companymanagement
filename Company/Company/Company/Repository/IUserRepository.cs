﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ekompania.Models;

namespace Company.Repository
{
    public interface IUserRepository
    {
        Task<User> GetUser(string email);
        Task<User> GetUser(Guid id);
        Task RemoveUser(string email);

        Task AddUser(String login, String haslo, String imie, String nazwisko, String stopien);

        Task<IEnumerable<User>> GetAllUsers();
    }
}
